
import express from 'express'
import createApp from '../src/server/create-app'
import getConfig from '../src/shared/config'

const state: any = {
    server: null,
    sockets: [],
}

/**
 * @param isRestart, 为 false 时, 才build/watch前端静态资源
 */
export function start(isRestart = false) {
    const config = getConfig()

    createApp(config, isRestart).then(({ app }) => {
        // 404
        app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
            res.status(404)
            res.send('Not Found')
        })

        // error catch
        app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
            res.status(err.status || 500);
            res.send(err.stack);
        })

        // 启动 server
        if (config.useHttp2 && config.tslFiles) {
            const fs = require('fs')
            const spdy = require('spdy')
            const options = {
                key: fs.readFileSync(config.tslFiles.key),
                cert: fs.readFileSync(config.tslFiles.cert),
            }
            state.server = spdy.createServer(options, app).listen(config.port, (error: any) => {
                if (error) {
                    console.error(error)
                    process.exit(1)
                } else {
                    console.info(`http2 server started on ${config.port}`);
                }
            })
        } else {
            state.server = app.listen(config.port, () => {
                console.info(`http server started on ${config.port}`);
            })
        }

        state.server.on('connection', (socket: any) => {
            state.sockets.push(socket);
        })
    }).catch(err => console.log(err))
}

function pathCheck(id: any) {
    return id.indexOf('node_modules') === -1
}

let timeoutInstance: any;

export function restart() {
    if (timeoutInstance) {
        clearTimeout(timeoutInstance);
    }

    timeoutInstance = setTimeout(function () {
        // clean the cache
        Object.keys(require.cache).forEach((id) => {
            if (pathCheck(id)) {
                delete require.cache[id];
            }
        })

        state.sockets.forEach((socket: any) => {
            if (socket.destroyed === false) {
                socket.destroy();
            }
        })

        state.sockets = [];

        state.server.close(() => {
            console.info('\nServer is closed');
            console.info('----------------- restarting -------------\n');
            start(true);
        });

    }, 1000);
}
