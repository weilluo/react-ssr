import createApp from '../src/client/create-app'
// @ts-ignore
import routes from 'root/src/pages'

createApp(routes).then(app => app?.start())
