"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var webpack_1 = __importDefault(require("webpack"));
var fs_extra_1 = __importDefault(require("fs-extra"));
var path_1 = __importDefault(require("path"));
var babel_config_1 = __importDefault(require("./babel-config"));
/**
 * 为了将资源名写入 assets.json, 提供给页面模版
 */
var AssetMapPlugin = /** @class */ (function () {
    function AssetMapPlugin(_a) {
        var dir = _a.dir;
        this.dir = dir;
    }
    AssetMapPlugin.prototype.apply = function (complier) {
        var _this = this;
        complier.hooks.afterEmit.tap('AfterEmitPlugin', function (compilation) {
            var assets = {};
            for (var key in compilation.assets) {
                if (key.indexOf('.map') === -1) {
                    if (key.indexOf('index') > -1) {
                        assets.index = key;
                    }
                    else if (key.indexOf('vendor') > -1) {
                        assets.vendor = key;
                    }
                    else {
                        assets[key] = key;
                    }
                }
            }
            fs_extra_1.default.ensureDirSync(_this.dir);
            fs_extra_1.default.writeFileSync(_this.dir + '/assets.json', JSON.stringify(assets, null, 2), { encoding: 'utf-8' });
        });
    };
    return AssetMapPlugin;
}());
var createWebpackConfig = function (config) {
    var root = config.root, isDev = config.isDev, buildDir = config.buildDir;
    var webpackConfig = {
        mode: isDev ? 'development' : 'production',
        entry: {
            index: path_1.default.resolve(__dirname, '../../entry/client.js')
        },
        output: {
            filename: "[name]" + (isDev ? '' : '.[contenthash:8]') + ".js",
            path: root + buildDir + '/static/js',
            publicPath: config.publicPath + '/static/js/',
        },
        module: {
            rules: [
                {
                    test: /\.(j|t)s[x]?$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: babel_config_1.default()
                    }
                },
            ]
        },
        optimization: {
            splitChunks: {
                cacheGroups: {
                    vendors: {
                        test: /[\\/]node_modules[\\/]/,
                        name: 'vendor',
                        priority: -20,
                        chunks: 'all'
                    }
                }
            }
        },
        resolve: {
            extensions: ['.ts', '.tsx', '.js', '.jsx'],
            alias: {
                root: process.cwd()
            }
        },
        plugins: [
            new AssetMapPlugin({ dir: root + config.buildDir }),
            new webpack_1.default.HashedModuleIdsPlugin(),
        ]
    };
    if (isDev) {
        webpackConfig.devtool = 'source-map';
    }
    return webpackConfig;
};
exports.default = createWebpackConfig;
