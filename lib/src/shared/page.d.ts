import React from 'react';
import { ViewType, PageStore, StoreState } from '../types';
declare type PageContextType = {
    page: Page;
    isClient: boolean;
    isServer: boolean;
};
export declare const PageContext: React.Context<PageContextType>;
export default class Page {
    useSSR: boolean;
    styles: string[];
    View: ViewType;
    store: PageStore;
    unsubscribe?: any;
    pageViewHandler?: PageView;
    constructor();
    getInitialState(): Promise<{}>;
    componentDidMount(): void;
    componentWillUnmount(): void;
    init: (initialState: StoreState) => void;
    updateState(changes: any): void;
    refreshView(): void;
    renderView(): JSX.Element;
    renderEmptyView(): JSX.Element;
}
declare type PageViewProps = {
    View: ViewType;
    state: StoreState;
    useSSR: boolean;
};
declare class PageView extends React.Component<PageViewProps, React.ComponentState> {
    constructor(props: PageViewProps);
    data: any;
    static contextType: React.Context<PageContextType>;
    UNSAFE_componentWillUpdate(): void;
    render(): JSX.Element;
}
export {};
