"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PageContext = void 0;
var react_1 = __importDefault(require("react"));
exports.PageContext = react_1.default.createContext({});
var Page = /** @class */ (function () {
    function Page() {
        var _this = this;
        this.useSSR = true;
        this.styles = [];
        // 初始化 store
        this.init = function (initialState) {
            _this.store = createStore(__assign({}, initialState));
            // 客户端初始化时, 订阅 state 的改变, 回掉函数中更新 View
            if (typeof window !== 'undefined') {
                // unsubscribe
                var unsubscribe = _this.store.subscribe(function () {
                    _this.refreshView();
                });
                // @ts-ignore
                _this.unsubscribe = unsubscribe;
            }
        };
    }
    // 服务端 hook, 执行 state 初始化
    Page.prototype.getInitialState = function () {
        return __awaiter(this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/, {}];
        }); });
    };
    // 客户端 hook
    Page.prototype.componentDidMount = function () { };
    // 客户端 hook
    Page.prototype.componentWillUnmount = function () { };
    // 更新state
    Page.prototype.updateState = function (changes) {
        this.store.dispatch(changes);
    };
    // 刷新View
    Page.prototype.refreshView = function () {
        var state = this.store.getState();
        this.pageViewHandler.setState(state);
    };
    // 渲染View
    Page.prototype.renderView = function () {
        var _this = this;
        var View = this.View;
        var state = this.store.getState();
        return (react_1.default.createElement(exports.PageContext.Provider, { value: getPageContext(this) },
            react_1.default.createElement(PageView, { ref: function (v) { return _this.pageViewHandler = v; }, state: state, View: View, useSSR: this.useSSR }),
            react_1.default.createElement(PageProxy, { page: this })));
    };
    // 渲染空View
    Page.prototype.renderEmptyView = function () {
        return react_1.default.createElement(react_1.default.Fragment, null);
    };
    return Page;
}());
exports.default = Page;
var PageView = /** @class */ (function (_super) {
    __extends(PageView, _super);
    function PageView(props) {
        var _this = _super.call(this, props) || this;
        _this.state = __assign({}, props.state);
        _this.data = { isFirstRender: true };
        return _this;
    }
    PageView.prototype.UNSAFE_componentWillUpdate = function () {
        this.data.isFirstRender = false;
    };
    PageView.prototype.render = function () {
        var _a = this.props, View = _a.View, useSSR = _a.useSSR;
        var state = this.state;
        // 客户端时非服务端渲染, 第一次渲染空View
        if (this.context.isClient && !useSSR && this.data.isFirstRender) {
            return react_1.default.createElement(react_1.default.Fragment, null);
        }
        return react_1.default.createElement(View, { state: state });
    };
    PageView.contextType = exports.PageContext;
    return PageView;
}(react_1.default.Component));
var PageProxy = /** @class */ (function (_super) {
    __extends(PageProxy, _super);
    function PageProxy() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PageProxy.prototype.componentDidMount = function () {
        var page = this.props.page;
        if (typeof page.componentDidMount === 'function') {
            page.componentDidMount();
        }
    };
    PageProxy.prototype.componentWillUnmount = function () {
        var page = this.props.page;
        if (typeof page.componentWillUnmount === 'function') {
            page.componentWillUnmount();
        }
    };
    PageProxy.prototype.render = function () {
        return react_1.default.createElement(react_1.default.Fragment, null);
    };
    return PageProxy;
}(react_1.default.Component));
function getPageContext(page) {
    var isClient = typeof window !== 'undefined';
    return {
        page: page,
        isClient: isClient,
        isServer: !isClient
    };
}
function createStore(initialState) {
    var state = __assign({}, initialState);
    var listeners = [];
    var subscribe = function (listener) {
        var length = listeners.push(listener);
        // unsubscribe, 返回给页面, 自行决定执行
        return function () {
            listeners.splice(length - 1, 1);
        };
    };
    var getState = function () {
        return state;
    };
    var dispatch = function (changes) {
        state = __assign(__assign({}, state), changes);
        listeners.forEach(function (listener) { return listener(); });
    };
    return {
        subscribe: subscribe,
        getState: getState,
        dispatch: dispatch,
    };
}
