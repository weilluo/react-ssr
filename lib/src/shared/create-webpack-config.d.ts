import webpack from 'webpack';
import { AppConfig } from '../types';
declare const createWebpackConfig: (config: AppConfig) => webpack.Configuration;
export default createWebpackConfig;
