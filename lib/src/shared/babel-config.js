"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function babelConfig(isServer) {
    if (isServer === void 0) { isServer = false; }
    var result = {
        presets: [
            [
                "@babel/preset-typescript",
                {
                    isTSX: true,
                    allExtensions: true,
                }
            ],
            "@babel/preset-env",
            "@babel/preset-react",
        ],
        plugins: [
            '@babel/plugin-transform-runtime',
            '@babel/plugin-syntax-dynamic-import',
            '@babel/plugin-proposal-class-properties',
            '@babel/plugin-proposal-nullish-coalescing-operator',
        ]
    };
    if (isServer) {
        result.plugins.push('dynamic-import-node');
    }
    return result;
}
exports.default = babelConfig;
