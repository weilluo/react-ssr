"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
function getConfig() {
    var env = process.env.NODE_ENV || 'development';
    // 项目根目录
    var root = process.cwd();
    var defaultConfig = {
        env: env,
        isDev: env === 'development',
        isServer: typeof window === 'undefined',
        isClient: typeof window !== 'undefined',
        port: 4001,
        basename: '/',
        publicPath: '',
        root: root,
        buildDir: '/dist',
    };
    var customConfig = require(root + '/ssr.config.js');
    return __assign(__assign({}, defaultConfig), customConfig);
}
exports.default = getConfig;
