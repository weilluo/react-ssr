export default function babelConfig(isServer?: boolean): {
    presets: (string | (string | {
        isTSX: boolean;
        allExtensions: boolean;
    })[])[];
    plugins: string[];
};
