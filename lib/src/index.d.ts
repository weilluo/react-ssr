import _BasePage from './shared/page';
import _usePage from './hook/use-page';
import _usePageState from './hook/use-state';
export declare const Page: typeof _BasePage;
export declare const usePage: typeof _usePage;
export declare const usePageState: typeof _usePageState;
export declare const fetch: typeof globalThis.fetch;
