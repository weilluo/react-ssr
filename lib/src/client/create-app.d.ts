import { Route } from '../types';
declare type CreateApp = (routes: Route[]) => Promise<null | {
    page: any;
    start: () => void;
}>;
declare const createApp: CreateApp;
export default createApp;
