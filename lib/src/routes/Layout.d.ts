/// <reference types="react" />
import { LayoutProps } from '../types';
export default function Layout(props: LayoutProps): JSX.Element;
