"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
function Layout(props) {
    var initialState = props.initialState, assets = props.assets, publicPath = props.publicPath, styleContents = props.styleContents;
    return (react_1.default.createElement("html", null,
        react_1.default.createElement("head", null, styleContents === null || styleContents === void 0 ? void 0 :
            styleContents.map(function (styleContent, index) {
                return react_1.default.createElement("style", { key: index, type: "text/css", dangerouslySetInnerHTML: { __html: styleContent } });
            }),
            react_1.default.createElement("title", null, initialState.title)),
        react_1.default.createElement("body", null,
            react_1.default.createElement("div", { id: "root", dangerouslySetInnerHTML: { __html: props.bodyContent || '' } }),
            react_1.default.createElement("script", { dangerouslySetInnerHTML: { __html: props.scriptContent } }),
            react_1.default.createElement("script", { src: publicPath + "/js/" + assets.index }),
            react_1.default.createElement("script", { src: publicPath + "/js/" + assets.vendor }))));
}
exports.default = Layout;
