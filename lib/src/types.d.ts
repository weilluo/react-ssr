import React from 'react';
import express from 'express';
export declare type Assets = any;
export declare type AppConfig = {
    /**
     * 服务根path
     */
    basename: string | string[];
    /**
     * 运行环境
     */
    env: string;
    /**
     * 是否开发环境
     */
    isDev: boolean;
    /**
     * 是否Client
     */
    isClient: boolean;
    /**
     * 是否Client
     */
    isServer: boolean;
    /**
     * 服务端口号
     */
    port: number;
    /**
     * 静态资源链接, 为 '' 则取本地资源
     */
    publicPath: string;
    /**
     * 项目根目录
     */
    root: string;
    /**
     * 打包文件目录
     */
    buildDir: string;
    /**
     * 使用 less
     */
    useLess?: boolean;
    /**
     * 使用 Sass
     */
    useSass?: boolean;
    /**
     * 使用 http2
     */
    useHttp2?: boolean;
    /**
     * https 证书
     */
    tslFiles?: {
        key: string;
        cert: string;
    };
};
export declare type Route = {
    path: string;
    getPage: () => Promise<any>;
};
export declare type CreateRouterParam = {
    app: express.Application;
    config: AppConfig;
    assets: Assets;
};
export declare type CreateServiceRouterParam = {
    app: express.Application;
    config: AppConfig;
};
export declare type RenderViewParams = {
    initialState: StoreState;
    assets: Assets;
    publicPath: string;
    styleContents?: string[];
    View: JSX.Element;
    config: AppConfig;
};
export declare type LayoutProps = {
    initialState: StoreState;
    assets: Assets;
    publicPath: string;
    styleContents?: string[];
    bodyContent: string;
    scriptContent: string;
};
export declare type StoreState = any;
export declare type Listener = () => void;
export interface PageStore {
    subscribe(listener: Listener): () => void;
    getState(): StoreState;
    dispatch(changes: StoreState): void;
}
export declare type PageProps = {
    state?: StoreState;
};
declare type PageViewClass = React.ComponentClass<PageProps, React.ComponentState>;
declare type PageViewJSX = () => JSX.Element;
export declare type ViewType = PageViewJSX | PageViewClass;
export {};
