/// <reference types="react" />
import Page from '../../shared/page';
declare function View(): JSX.Element;
export default class Demo extends Page {
    View: typeof View;
    styles: string[];
    useSSR: boolean;
    getInitialState: () => Promise<{
        title: string;
        number: number;
    }>;
    plusNumber: () => void;
}
export {};
