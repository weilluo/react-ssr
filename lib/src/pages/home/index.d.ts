/// <reference types="react" />
import Page from '../../shared/page';
declare function View(): JSX.Element;
export default class Home extends Page {
    View: typeof View;
    styles: string[];
    getInitialState: () => Promise<{
        title: string;
        buttonName: string;
    }>;
}
export {};
