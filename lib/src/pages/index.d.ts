declare const _default: ({
    path: string;
    getPage: () => Promise<typeof import("./home")>;
} | {
    path: string;
    getPage: () => Promise<typeof import("./about")>;
})[];
export default _default;
