"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_extra_1 = __importDefault(require("fs-extra"));
var glob_1 = __importDefault(require("glob"));
var render_view_1 = __importDefault(require("./render-view"));
// 创建页面 Router
var createRouter = function (_a) {
    var app = _a.app, config = _a.config, assets = _a.assets;
    // 页面路由
    var routes = require(config.root + (config.isDev ? '' : config.buildDir) + '/src/pages');
    routes = (routes.default || routes);
    app.use(function (req, res, next) {
        var _a;
        // 页面 router 接收 get 请求
        if (!/get/i.test(req.method)) {
            return next();
        }
        var bases = typeof config.basename === 'string' ? [config.basename] : config.basename;
        // xxx.com/xxx/xxx
        var basename = (_a = req.path.match(/\/\w+/)) === null || _a === void 0 ? void 0 : _a[0];
        // xxx.com/
        // xxx.com/xxx, 其中 /xxx 也不是 basename
        // 则认为 basename 为 '/'
        if (bases.indexOf(basename) === -1) {
            basename = '/';
        }
        var route = routes.find(function (r) {
            var path;
            if (basename === '/') {
                path = r.path;
            }
            else {
                path = basename;
                // 非前端首页时
                if (r.path !== '/') {
                    path = basename + r.path;
                }
            }
            return req.path === path;
        });
        if (!route) {
            return next();
        }
        route.getPage().then(function (result) { return __awaiter(void 0, void 0, void 0, function () {
            var Page, page, NODE_ENV, env, cssFilePaths, styleContents, initialState, View, publicPath;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        Page = result.default || result;
                        page = new Page();
                        NODE_ENV = process.env.NODE_ENV;
                        env = NODE_ENV === 'production' ? 'prod' : (NODE_ENV === 'test' ? 'test' : 'dev');
                        return [4 /*yield*/, findCssFiles(config.root + config.buildDir + '/static/css', page.styles || [])
                            // 样式表内容
                        ];
                    case 1:
                        cssFilePaths = _a.sent();
                        styleContents = cssFilePaths === null || cssFilePaths === void 0 ? void 0 : cssFilePaths.map(function (cssFilePath) {
                            var content = fs_extra_1.default.readFileSync(cssFilePath);
                            return content.toString();
                        });
                        return [4 /*yield*/, page.getInitialState()
                            // 将 currentPath / basename / env 放入初始 State
                        ];
                    case 2:
                        initialState = _a.sent();
                        // 将 currentPath / basename / env 放入初始 State
                        page.init(__assign(__assign({}, initialState), { currentPath: route.path, basename: basename,
                            env: env }));
                        View = page.useSSR ? page.renderView() : page.renderEmptyView();
                        publicPath = config.publicPath;
                        if (!publicPath) {
                            publicPath = (basename === '/' ? '' : basename) + '/static';
                        }
                        res.send(render_view_1.default({
                            initialState: page.store.getState(),
                            assets: assets,
                            styleContents: styleContents,
                            publicPath: publicPath,
                            View: View,
                            config: config
                        }));
                        return [2 /*return*/];
                }
            });
        }); }).catch(function (err) { return next(err); });
    });
};
exports.default = createRouter;
var EmptyArray = [];
// 在静态资源中，寻找页面配置的 css 文件
function findCssFiles(dir, styles) {
    return new Promise(function (resolve, reject) {
        glob_1.default(dir + '/**/*.css', {}, function (err, filePaths) {
            if (err) {
                resolve(EmptyArray);
                return;
            }
            var cssFilePaths = filePaths.filter(function (filePath) {
                // .../client/css/home/home.118a96dd.css
                var fileName = filePath.split(/\\|\//).pop().split('.css')[0];
                return styles.filter(function (styleName) { return fileName.indexOf(styleName) === 0; }).length > 0;
            });
            resolve(cssFilePaths);
        });
    });
}
