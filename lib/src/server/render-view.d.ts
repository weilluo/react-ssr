import { RenderViewParams } from '../types';
declare const renderView: (props: RenderViewParams) => string;
export default renderView;
