import { CreateServiceRouterParam } from '../types';
declare const createServiceRouter: ({ app, config }: CreateServiceRouterParam) => void;
export default createServiceRouter;
