"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = __importDefault(require("path"));
var fs_extra_1 = __importDefault(require("fs-extra"));
var express_1 = __importDefault(require("express"));
var create_router_1 = __importDefault(require("./create-router"));
var create_service_router_1 = __importDefault(require("./create-service-router"));
// 单位: 秒
var ensureAssetsTimeOut = 5;
var defaultAssets = {
    index: 'index.js',
    vendor: 'vendor.js'
};
var ensureAssets = function (config) {
    return new Promise(function (resolve, reject) {
        var assetsPath = config.root + config.buildDir + '/assets.json';
        var time = 0;
        var _interval = setInterval(function () {
            if (fs_extra_1.default.pathExistsSync(assetsPath)) {
                clearInterval(_interval);
                var content = fs_extra_1.default.readJsonSync(assetsPath, { encoding: 'utf-8' });
                if (Object.keys(content).length > 0) {
                    return resolve(content);
                }
                else {
                    return reject('assets file is invalid');
                }
            }
            if (time++ >= ensureAssetsTimeOut) {
                clearInterval(_interval);
                if (config.isDev) {
                    resolve(defaultAssets);
                }
                else {
                    reject('not fount assets file');
                }
            }
        }, 1000);
    });
};
var createApp = function (config, isRestart) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/, ensureAssets(config).then(function (assets) { return __awaiter(void 0, void 0, void 0, function () {
                var app, wmdClose, webpack, webpackDevMiddleware, createWebpackConfig, compiler, path_2, child_process, forked, forked, staticPath;
                return __generator(this, function (_a) {
                    app = express_1.default();
                    if (config.isDev && !isRestart) {
                        webpack = require('webpack');
                        webpackDevMiddleware = require('webpack-dev-middleware');
                        createWebpackConfig = require(path_1.default.resolve(__dirname, '../shared/create-webpack-config'));
                        createWebpackConfig = createWebpackConfig.default || createWebpackConfig;
                        compiler = webpack(createWebpackConfig(config));
                        app.use(webpackDevMiddleware(compiler, { writeToDisk: true }));
                        if (config.useLess || config.useSass) {
                            path_2 = require('path');
                            child_process = require('child_process');
                            // 处理 less
                            if (config.useLess) {
                                forked = child_process.fork(path_2.resolve(__dirname, '../../../lib/src/shared/less.js'));
                                forked.send({ watch: true });
                            }
                            // 处理 sass
                            if (config.useSass) {
                                forked = child_process.fork(path_2.resolve(__dirname, '../../../lib/src/shared/sass.js'));
                                forked.send({ watch: true });
                            }
                        }
                    }
                    staticPath = typeof config.basename === 'string' ? [config.basename] : config.basename;
                    staticPath.forEach(function (p) {
                        app.use("" + p + (p.endsWith('/') ? '' : '/') + "static", express_1.default.static(path_1.default.resolve(config.root, 'dist/static')));
                    });
                    // 创建页面路由
                    create_router_1.default({ app: app, config: config, assets: assets });
                    // 创建服务路由
                    create_service_router_1.default({ app: app, config: config });
                    return [2 /*return*/, { app: app, wmdClose: wmdClose }];
                });
            }); })];
    });
}); };
exports.default = createApp;
