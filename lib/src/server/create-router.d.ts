import { CreateRouterParam } from '../types';
declare const createRouter: ({ app, config, assets }: CreateRouterParam) => void;
export default createRouter;
