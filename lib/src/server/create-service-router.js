"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var createServiceRouter = function (_a) {
    var _b;
    var app = _a.app, config = _a.config;
    // 服务路由
    var routes;
    try {
        routes = require(config.root + (config.isDev ? '' : config.buildDir) + '/src/routes');
        routes = routes.default || routes;
    }
    catch (err) { }
    if (!routes)
        return;
    //服务路由过滤器
    app.use(function (req, res, next) {
        var _a;
        // 服务 router 接收 post 请求
        if (!/post/i.test(req.method)) {
            return next();
        }
        var bases = typeof config.basename === 'string' ? [config.basename] : config.basename;
        // 取 xxx.com/xxx/xxx 的第一个 /xxx
        var basename = (_a = req.path.match(/\/\w+/)) === null || _a === void 0 ? void 0 : _a[0];
        // xxx.com/
        // xxx.com/xxx, 其中 /xxx 也不是 basename
        // 则认为认为 basename 为 '/'
        if (bases.indexOf(basename) === -1) {
            basename = '/';
        }
        if (basename === '/') {
            next();
        }
        else {
            // 因为 service route 中不写 basename,
            // 替换掉请求链接中 basename 部分后, 再内部请求真正的 service route
            req.url = req.url.replace(basename, '');
            next('route');
        }
    });
    // 加载服务路由
    for (var key in routes) {
        (_b = routes[key]) === null || _b === void 0 ? void 0 : _b.call(routes, app);
    }
};
exports.default = createServiceRouter;
