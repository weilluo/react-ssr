"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var server_1 = require("react-dom/server");
var renderView = function (props) {
    var initialState = props.initialState, assets = props.assets, config = props.config, styleContents = props.styleContents, publicPath = props.publicPath, View = props.View;
    var bodyContent = server_1.renderToString(View);
    var scriptContent = "\n        (function() {\n            window.__INITIAL_STATE__ = " + JSON.stringify(initialState) + "\n        })()\n    ";
    var Layout = require(config.root + (config.isDev ? '' : config.buildDir) + '/src/routes/Layout');
    Layout = Layout.default || Layout;
    var html = server_1.renderToStaticMarkup(Layout({
        initialState: initialState,
        assets: assets,
        publicPath: publicPath,
        styleContents: styleContents,
        bodyContent: bodyContent,
        scriptContent: scriptContent,
    }));
    return html;
};
exports.default = renderView;
