import express from 'express';
import { AppConfig } from '../types';
declare type CreateApp = (config: AppConfig, isRestart?: boolean) => Promise<{
    app: express.Application;
}>;
declare const createApp: CreateApp;
export default createApp;
