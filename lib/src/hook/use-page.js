"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = require("react");
var page_1 = require("../shared/page");
function usePage() {
    var page = react_1.useContext(page_1.PageContext).page;
    return page;
}
exports.default = usePage;
