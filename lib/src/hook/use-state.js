"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var use_page_1 = __importDefault(require("./use-page"));
function useState() {
    var page = use_page_1.default();
    return page.store.getState();
}
exports.default = useState;
