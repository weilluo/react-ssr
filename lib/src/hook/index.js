"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.usePageState = exports.usePage = void 0;
var use_page_1 = __importDefault(require("./use-page"));
var use_state_1 = __importDefault(require("./use-state"));
exports.usePage = use_page_1.default;
exports.usePageState = use_state_1.default;
