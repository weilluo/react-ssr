import _usePage from './use-page';
import _usePageState from './use-state';
export declare const usePage: typeof _usePage;
export declare const usePageState: typeof _usePageState;
