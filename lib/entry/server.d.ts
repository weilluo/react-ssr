/**
 * @param isRestart, 为 false 时, 才build/watch前端静态资源
 */
export declare function start(isRestart?: boolean): void;
export declare function restart(): void;
