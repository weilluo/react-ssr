"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.restart = exports.start = void 0;
var create_app_1 = __importDefault(require("../src/server/create-app"));
var config_1 = __importDefault(require("../src/shared/config"));
var state = {
    server: null,
    sockets: [],
};
/**
 * @param isRestart, 为 false 时, 才build/watch前端静态资源
 */
function start(isRestart) {
    if (isRestart === void 0) { isRestart = false; }
    var config = config_1.default();
    create_app_1.default(config, isRestart).then(function (_a) {
        var app = _a.app;
        // 404
        app.use(function (req, res, next) {
            res.status(404);
            res.send('Not Found');
        });
        // error catch
        app.use(function (err, req, res, next) {
            res.status(err.status || 500);
            res.send(err.stack);
        });
        // 启动 server
        if (config.useHttp2 && config.tslFiles) {
            var fs = require('fs');
            var spdy = require('spdy');
            var options = {
                key: fs.readFileSync(config.tslFiles.key),
                cert: fs.readFileSync(config.tslFiles.cert),
            };
            state.server = spdy.createServer(options, app).listen(config.port, function (error) {
                if (error) {
                    console.error(error);
                    process.exit(1);
                }
                else {
                    console.info("http2 server started on " + config.port);
                }
            });
        }
        else {
            state.server = app.listen(config.port, function () {
                console.info("http server started on " + config.port);
            });
        }
        state.server.on('connection', function (socket) {
            state.sockets.push(socket);
        });
    }).catch(function (err) { return console.log(err); });
}
exports.start = start;
function pathCheck(id) {
    return id.indexOf('node_modules') === -1;
}
var timeoutInstance;
function restart() {
    if (timeoutInstance) {
        clearTimeout(timeoutInstance);
    }
    timeoutInstance = setTimeout(function () {
        // clean the cache
        Object.keys(require.cache).forEach(function (id) {
            if (pathCheck(id)) {
                delete require.cache[id];
            }
        });
        state.sockets.forEach(function (socket) {
            if (socket.destroyed === false) {
                socket.destroy();
            }
        });
        state.sockets = [];
        state.server.close(function () {
            console.info('\nServer is closed');
            console.info('----------------- restarting -------------\n');
            start(true);
        });
    }, 1000);
}
exports.restart = restart;
