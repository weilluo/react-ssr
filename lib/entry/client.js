"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var create_app_1 = __importDefault(require("../src/client/create-app"));
// @ts-ignore
var pages_1 = __importDefault(require("root/src/pages"));
create_app_1.default(pages_1.default).then(function (app) { return app === null || app === void 0 ? void 0 : app.start(); });
