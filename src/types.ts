import React from 'react'
import express from 'express'

// js资源映射关系
export type Assets = any

export type AppConfig = {
    /**
     * 服务根path
     */
    basename: string | string[]

    /**
     * 运行环境
     */
    env: string

    /**
     * 是否开发环境
     */
    isDev: boolean

    /**
     * 是否Client
     */
    isClient: boolean

    /**
     * 是否Client
     */
    isServer: boolean

    /**
     * 服务端口号
     */
    port: number

    /**
     * 静态资源链接, 为 '' 则取本地资源
     */
    publicPath: string

    /**
     * 项目根目录
     */
    root: string

    /**
     * 打包文件目录
     */
    buildDir: string

    /**
     * 使用 less
     */
    useLess?: boolean

    /**
     * 使用 Sass
     */
    useSass?: boolean

    /**
     * 使用 http2
     */
    useHttp2?: boolean

    /**
     * https 证书
     */
    tslFiles?: {
        key: string,
        cert: string,
    }
}

export type Route = {
    path: string
    getPage: () => Promise<any>
}

export type CreateRouterParam = {
    app: express.Application,
    config: AppConfig,
    assets: Assets,
}

export type CreateServiceRouterParam = {
    app: express.Application,
    config: AppConfig,
}

export type RenderViewParams = {
    initialState: StoreState
    assets: Assets
    publicPath: string
    styleContents?: string[]
    View: JSX.Element
    config: AppConfig
}

export type LayoutProps = {
    initialState: StoreState
    assets: Assets
    publicPath: string
    styleContents?: string[]
    // 页面内容
    bodyContent: string
    scriptContent: string
}

export type StoreState = any

export type Listener = () => void

// Page 相关
export interface PageStore {
    subscribe(listener: Listener): () => void

    getState(): StoreState

    dispatch(changes: StoreState): void
}

export type PageProps = {
    state?: StoreState
}

type PageViewClass = React.ComponentClass<PageProps, React.ComponentState>
type PageViewJSX = () => JSX.Element

export type ViewType = PageViewJSX | PageViewClass
