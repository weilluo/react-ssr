import express from 'express'

import { CreateServiceRouterParam } from '../types'

const createServiceRouter = ({ app, config }: CreateServiceRouterParam) => {
    // 服务路由
    let routes: any
    try {
        routes = require(config.root + (config.isDev ? '' : config.buildDir) + '/src/routes')
        routes = routes.default || routes;
    } catch (err) {}

    if (!routes) return

    //服务路由过滤器
    app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {

        // 服务 router 接收 post 请求
        if (!/post/i.test(req.method)) {
            return next()
        }

        let bases = typeof config.basename === 'string' ? [config.basename] : config.basename;

        // 取 xxx.com/xxx/xxx 的第一个 /xxx
        let basename = req.path.match(/\/\w+/)?.[0]

        // xxx.com/
        // xxx.com/xxx, 其中 /xxx 也不是 basename
        // 则认为认为 basename 为 '/'
        if (bases.indexOf(basename) === -1) {
            basename = '/'
        }

        if (basename === '/') {
            next()
        } else {
            // 因为 service route 中不写 basename,
            // 替换掉请求链接中 basename 部分后, 再内部请求真正的 service route
            req.url = req.url.replace(basename, '')
            next('route')
        }
    })

    // 加载服务路由
    for (let key in routes) {
        routes[key]?.(app)
    }
}

export default createServiceRouter;
