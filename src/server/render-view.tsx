import { renderToString, renderToStaticMarkup } from 'react-dom/server'
import { RenderViewParams } from '../types'

const renderView = (props: RenderViewParams) => {

    const { initialState, assets, config, styleContents, publicPath, View } = props

    let bodyContent: string = renderToString(View)

    const scriptContent = `
        (function() {
            window.__INITIAL_STATE__ = ${JSON.stringify(initialState)}
        })()
    `;

    let Layout = require(config.root + (config.isDev ? '' : config.buildDir) + '/src/routes/Layout')
    Layout = Layout.default || Layout;

    const html = renderToStaticMarkup(Layout({
        initialState,
        assets,
        publicPath,
        styleContents,
        bodyContent,
        scriptContent,
    }))

    return html
}

export default renderView
