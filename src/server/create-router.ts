import express from 'express'
import fs from 'fs-extra'
import glob from 'glob'

import renderView from './render-view'
import { CreateRouterParam, Route } from '../types'

// 创建页面 Router
const createRouter = ({ app, config, assets }: CreateRouterParam) => {
    // 页面路由
    let routes = require(config.root + (config.isDev ? '' : config.buildDir) + '/src/pages')
    routes = (routes.default || routes)

    app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {

        // 页面 router 接收 get 请求
        if (!/get/i.test(req.method)) {
            return next()
        }

        let bases = typeof config.basename === 'string' ? [config.basename] : config.basename;

        // xxx.com/xxx/xxx
        let basename = req.path.match(/\/\w+/)?.[0]

        // xxx.com/
        // xxx.com/xxx, 其中 /xxx 也不是 basename
        // 则认为 basename 为 '/'
        if (bases.indexOf(basename) === -1) {
            basename = '/'
        }

        let route = routes.find((r: Route) => {
            let path

            if (basename === '/') {
                path = r.path
            } else {
                path = basename
                // 非前端首页时
                if (r.path !== '/') {
                    path = basename + r.path
                }
            }

            return req.path === path
        })

        if (!route) {
            return next()
        }

        route.getPage().then(async (result: any) => {
            const Page = result.default || result

            const page = new Page()

            const NODE_ENV = process.env.NODE_ENV
            const env = NODE_ENV === 'production' ? 'prod' : (NODE_ENV === 'test' ? 'test' : 'dev');

            const cssFilePaths = await findCssFiles(config.root + config.buildDir + '/static/css', page.styles || [])

            // 样式表内容
            const styleContents = cssFilePaths?.map(cssFilePath => {
                const content = fs.readFileSync(cssFilePath)
                return content.toString()
            })

            const initialState = await page.getInitialState()

            // 将 currentPath / basename / env 放入初始 State
            page.init({
                ...initialState,
                currentPath: route.path,
                basename,
                env
            })
            // 非服务端渲染时, 返回空View
            const View = page.useSSR ? page.renderView() : page.renderEmptyView();

            let publicPath = config.publicPath

            if (!publicPath) {
                publicPath = (basename === '/' ? '' : basename) + '/static'
            }

            res.send(renderView(
                {
                    initialState: page.store.getState(),
                    assets,
                    styleContents,
                    publicPath,
                    View,
                    config
                },
            ))
        }).catch((err: any) => next(err))
    })

}

export default createRouter;

const EmptyArray: string[] = []

// 在静态资源中，寻找页面配置的 css 文件
function findCssFiles(dir: string, styles: string[]): Promise<string[]> {

    return new Promise((resolve, reject) => {

        glob(dir + '/**/*.css', {}, (err, filePaths) => {
            if (err) {
                resolve(EmptyArray);
                return;
            }

            let cssFilePaths = filePaths.filter(filePath => {
                // .../client/css/home/home.118a96dd.css
                let fileName = filePath.split(/\\|\//).pop().split('.css')[0];

                return styles.filter(styleName => fileName.indexOf(styleName) === 0).length > 0;

            });

            resolve(cssFilePaths);
        })

    })

}
