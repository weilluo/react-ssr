import path from 'path'
import fs from 'fs-extra'
import express from 'express'

import createRouter from './create-router'
import createServiceRouter from './create-service-router'
import { AppConfig, Assets } from '../types'

// 单位: 秒
const ensureAssetsTimeOut = 5
const defaultAssets: Assets = {
    index: 'index.js',
    vendor: 'vendor.js'
}

type EnsureAssets = (config: AppConfig) => Promise<Assets>

const ensureAssets: EnsureAssets = (config: AppConfig) => {
    return new Promise((resolve, reject) => {

        const assetsPath = config.root + config.buildDir + '/assets.json'

        let time = 0

        let _interval = setInterval(() => {
            if (fs.pathExistsSync(assetsPath)) {
                clearInterval(_interval);
                let content = fs.readJsonSync(assetsPath, { encoding: 'utf-8' });
                if (Object.keys(content).length > 0) {
                    return resolve(content)
                } else {
                    return reject('assets file is invalid')
                }
            }

            if (time++ >= ensureAssetsTimeOut) {
                clearInterval(_interval)
                if (config.isDev) {
                    resolve(defaultAssets)
                } else {
                    reject('not fount assets file')
                }
            }
        }, 1000);
    })
}

type CreateApp = (config: AppConfig, isRestart?: boolean) => Promise<{ app: express.Application }>

const createApp: CreateApp = async (config: AppConfig, isRestart?: boolean) => {
    return ensureAssets(config).then(async assets => {
        const app = express()

        let wmdClose: any

        if (config.isDev && !isRestart) {
            const webpack = require('webpack')
            const webpackDevMiddleware = require('webpack-dev-middleware')

            let createWebpackConfig = require(path.resolve(__dirname, '../shared/create-webpack-config'))
            createWebpackConfig = createWebpackConfig.default || createWebpackConfig

            const compiler = webpack(createWebpackConfig(config))
            app.use(webpackDevMiddleware(compiler, { writeToDisk: true }))

            if (config.useLess || config.useSass) {
                const path = require('path')
                const child_process = require('child_process')

                // 处理 less
                if (config.useLess) {
                    const forked = child_process.fork(path.resolve(__dirname, '../../../lib/src/shared/less.js'))
                    forked.send({ watch: true })
                }

                // 处理 sass
                if (config.useSass) {
                    const forked = child_process.fork(path.resolve(__dirname, '../../../lib/src/shared/sass.js'))
                    forked.send({ watch: true })
                }
            }
        }

        // 本地静态资源
        const staticPath = typeof config.basename === 'string' ? [config.basename] : config.basename
        staticPath.forEach(p => {
            app.use(`${p}${p.endsWith('/') ? '' : '/'}static`, express.static(path.resolve(config.root, 'dist/static')))
        })

        // 创建页面路由
        createRouter({ app, config, assets })

        // 创建服务路由
        createServiceRouter({ app, config })

        return { app, wmdClose }
    })
}

export default createApp;
