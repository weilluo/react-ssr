import React from 'react';
import { hydrate } from 'react-dom';
import { Route } from '../types';

type CreateApp = (routes: Route[]) => Promise<null | { page: any, start: () => void }>

const createApp: CreateApp = async (routes: Route[]) => {
    try {
        const Page = await getComponent(routes)

        // @ts-ignore
        let initialState = window.__INITIAL_STATE__ || {};
        // 清除同构state
        // @ts-ignore
        window.__INITIAL_STATE__ = undefined;

        let page = new Page()

        return {
            page,
            start: () => {
                if (page.useSSR) {
                    page.init(initialState)
                    hydrate(
                        page.renderView(),
                        document.getElementById('root')
                    )
                } else {
                    page.init()
                    hydrate(
                        page.renderView(),
                        document.getElementById('root'),
                        () => {
                            page.updateState(initialState)
                        }
                    )
                }
            }
        }

    } catch (err) {
        renderError(err)
        return null
    }
}

export default createApp;

type GetComponent = (routes: Route[]) => Promise<any>

const getComponent: GetComponent = (routes: Route[]) => {
    return new Promise((resolve, reject) => {

        const route = routes.filter(r => {
            // @ts-ignore
            return window.__INITIAL_STATE__.currentPath === r.path;
        })[0]

        if (route) {
            route.getPage().then(result => {
                const Page = result.default || result
                resolve(Page)
            })
            .catch(reject)
        } else {
            reject()
        }

    })
}

const renderError = (err: Error) => {
    hydrate(
        <React.Fragment>
            {err.message}
            {err.stack}
        </React.Fragment>,
        document.getElementById('root')
    )
}
