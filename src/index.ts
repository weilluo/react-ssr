import _BasePage from './shared/page'
import _usePage from './hook/use-page'
import _usePageState from './hook/use-state'
import _fetch from './shared/fetch'

export const Page = _BasePage
export const usePage = _usePage
export const usePageState = _usePageState
export const fetch = _fetch
