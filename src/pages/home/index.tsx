import React from 'react';
import Page from '../../shared/page';
import { usePageState } from '../../hook';

function View() {
    const state = usePageState()

    return (
        <div className='app'>
            {state.title}

            <button onClick={() => console.info(123)}>
                {state.buttonName}
            </button>

            <div className="div" style={{ marginTop: '20px' }}>
                123123
            </div>

            <div style={{ border: '1px solid #0086F6', marginTop: '20px' }}>
                23123123213213213
            </div>
        </div>
    )
}

export default class Home extends Page {
    View = View
    styles = ['home']

    getInitialState = async () => {
        return {
            title: 'Home',
            buttonName: 'Btn name',
        }
    }
}
