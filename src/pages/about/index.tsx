import React from 'react'
import Page from '../../shared/page'
import { usePage, usePageState } from '../../hook';

function View() {
    const page = usePage() as Demo
    const state = usePageState()

    return (
        <div className='about' onClick={page.plusNumber}>
            {state.title} {state.number}
        </div>
    )
}

export default class Demo extends Page {
    View = View
    styles = ['about']

    useSSR = false

    getInitialState = async () => {
        return {
            title: 'About',
            number: 1,
        }
    }

    plusNumber = () => {
        const state = this.store.getState()
        this.store.dispatch({ number: state.number + 1 })
    }
}
