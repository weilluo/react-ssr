import usePage from './use-page'

export default function useState() {
    const page = usePage()
    return page.store.getState()
}
