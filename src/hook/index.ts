import _usePage from './use-page'
import _usePageState from './use-state'

export const usePage = _usePage
export const usePageState = _usePageState
