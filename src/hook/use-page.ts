import { useContext } from 'react'
import { PageContext } from '../shared/page'

export default function usePage() {
    const { page } = useContext(PageContext)
    return page
}
