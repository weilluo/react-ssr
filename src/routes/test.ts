import express from 'express'

export function test(app: express.Application) {
    app.post('/test', (req, res) => {
        res.json({ test: 'test api' })
    })
}
