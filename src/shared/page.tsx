import React from 'react'
import { ViewType, PageStore, StoreState, Listener } from '../types';

type PageContextType = {
    page: Page
    isClient: boolean
    isServer: boolean
}

export const PageContext = React.createContext({} as PageContextType)

export default class Page {
    useSSR: boolean = true

    styles: string[] = []

    View: ViewType

    store: PageStore

    unsubscribe?: any

    pageViewHandler?: PageView

    constructor() {}

    // 服务端 hook, 执行 state 初始化
    async getInitialState() { return {} }

    // 客户端 hook
    componentDidMount() { }

    // 客户端 hook
    componentWillUnmount() { }

    // 初始化 store
    init = (initialState: StoreState) => {
        this.store = createStore({ ...initialState })

        // 客户端初始化时, 订阅 state 的改变, 回掉函数中更新 View
        if (typeof window !== 'undefined') {
            // unsubscribe
            let unsubscribe = this.store.subscribe(() => {
                this.refreshView()
            })
            // @ts-ignore
            this.unsubscribe = unsubscribe
        }
    }

    // 更新state
    updateState(changes: any) {
        this.store.dispatch(changes)
    }

    // 刷新View
    refreshView() {
        const state = this.store.getState()
        this.pageViewHandler.setState(state)
    }

    // 渲染View
    renderView() {
        const View = this.View
        const state = this.store.getState()

        return (
            <PageContext.Provider value={getPageContext(this)}>
                <PageView ref={v => this.pageViewHandler = v} state={state} View={View} useSSR={this.useSSR} />
                <PageProxy page={this} />
            </PageContext.Provider>
        )
    }

    // 渲染空View
    renderEmptyView() {
        return <React.Fragment />
    }
}

type PageViewProps = {
    View: ViewType
    state: StoreState
    useSSR: boolean
}

class PageView extends React.Component<PageViewProps, React.ComponentState> {
    constructor(props: PageViewProps) {
        super(props)
        this.state = { ...props.state }
        this.data = { isFirstRender: true }
    }

    data: any

    static contextType = PageContext

    UNSAFE_componentWillUpdate() {
        this.data.isFirstRender = false
    }

    render() {
        const { View, useSSR } = this.props
        const state = this.state

        // 客户端时非服务端渲染, 第一次渲染空View
        if (this.context.isClient && !useSSR && this.data.isFirstRender) {
            return <React.Fragment />
        }

        return <View state={state} /> 
    }
}

type PageProxyProps = {
    page: Page
}

class PageProxy extends React.Component<PageProxyProps, React.ComponentState> {

    componentDidMount() {
        const { page } = this.props
        if (typeof page.componentDidMount === 'function') {
            page.componentDidMount()
        }
    }

    componentWillUnmount() {
        const { page } = this.props
        if (typeof page.componentWillUnmount === 'function') {
            page.componentWillUnmount()
        }
    }

    render() {
        return <React.Fragment />
    }
}

function getPageContext(page: Page): PageContextType {
    const isClient = typeof window !== 'undefined'

    return {
        page,
        isClient,
        isServer: !isClient
    }
}

function createStore(initialState: StoreState): PageStore {

    let state = { ...initialState }

    let listeners: Listener[] = []

    let subscribe = (listener: Listener) => {

        let length = listeners.push(listener)

        // unsubscribe, 返回给页面, 自行决定执行
        return () => {
            listeners.splice(length - 1, 1)
        }
    }

    let getState = () => {
        return state
    }

    let dispatch = (changes: StoreState) => {
        state = { ...state, ...changes }
        listeners.forEach(listener => listener())
    }

    return {
        subscribe,
        getState,
        dispatch,
    }
}
