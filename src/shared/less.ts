// @ts-nocheck
import gulp from 'gulp'
import less from 'gulp-less'
import autoprefixer from 'gulp-autoprefixer'
import hash from 'gulp-hash-filename'

// app 根目录
const root = process.cwd();

process.on('message', (msg) => {
    if (!msg) return

    if (msg.watch) watch()
})

export const watch = () => {
    let cssSrc = `${root}/src/pages/**/*.less`;
    let cssDest = `${root}/dist/static/css`;

    gulp.watch(cssSrc, { ignoreInitial: false }, (cb) => {
        gulp.src(cssSrc)
            .pipe(less())
            .pipe(autoprefixer('last 60 versions'))
            .pipe(gulp.dest(cssDest))

        console.log('watch less!')

        cb()
    })
}

export default async function build() {
    return new Promise((resolve, reject) => {
        let cssSrc = `${root}/src/pages/**/*.less`;
        let cssDest = `${root}/dist/static/css`;

        let isSuccess = true

        gulp.src(cssSrc)
            .pipe(less())
            .pipe(autoprefixer('last 60 versions'))
            .pipe(hash({
                format: '{name}.{hash:8}{ext}'
            }))
            .pipe(gulp.dest(cssDest))
            .on('error', (err) => {
                isSuccess = false
                reject(err)
            })
            .on('end', () => {
                if (isSuccess) {
                    console.log('build less completed!')
                    resolve()
                }
            })
    })
}
