import { AppConfig } from "../types"

export default function getConfig() {
    const env = process.env.NODE_ENV || 'development'

    // 项目根目录
    const root = process.cwd()

    const defaultConfig: AppConfig = {
        env,
        isDev: env === 'development',
        isServer: typeof window === 'undefined',
        isClient: typeof window !== 'undefined',
        port: 4001,
        basename: '/',
        publicPath: '',
        root,
        buildDir: '/dist',
    }

    const customConfig = require(root + '/ssr.config.js')

    return { ...defaultConfig, ...customConfig } as AppConfig
}
