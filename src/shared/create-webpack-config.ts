import webpack from 'webpack'
import fs from 'fs-extra'
import path from 'path'
import { Assets, AppConfig } from '../types'
import babelConfig from './babel-config'

/**
 * 为了将资源名写入 assets.json, 提供给页面模版
 */
class AssetMapPlugin {
    dir: string

    constructor({ dir }: { dir: string }) {
        this.dir = dir;
    }

    apply(complier: webpack.Compiler) {
        complier.hooks.afterEmit.tap('AfterEmitPlugin', (compilation) => {

            const assets: Assets = {}

            for (let key in compilation.assets) {
                if (key.indexOf('.map') === -1) {
                    if (key.indexOf('index') > -1) {
                        assets.index = key;
                    } else if (key.indexOf('vendor') > -1) {
                        assets.vendor = key;
                    } else {
                        assets[key] = key;
                    }
                }
            }

            fs.ensureDirSync(this.dir);

            fs.writeFileSync(this.dir + '/assets.json', JSON.stringify(assets, null, 2), { encoding: 'utf-8' });
        });
    }
}

const createWebpackConfig = (config: AppConfig) => {

    const { root, isDev, buildDir } = config

    const webpackConfig: webpack.Configuration = {
        mode: isDev ? 'development' : 'production',

        entry: {
            index: path.resolve(__dirname, '../../entry/client.js')
        },

        output: {
            filename: `[name]${isDev ? '' : '.[contenthash:8]'}.js`,
            path: root + buildDir + '/static/js',
            publicPath: config.publicPath + '/static/js/',  // 分割文件引入时的 Path
        },

        module: {
            rules: [
                {
                    test: /\.(j|t)s[x]?$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: babelConfig()
                    }
                },
            ]
        },

        optimization: {
            splitChunks: {
                cacheGroups: {
                    vendors: {
                        test: /[\\/]node_modules[\\/]/,
                        name: 'vendor',
                        priority: -20,
                        chunks: 'all'
                    }
                }
            }
        },

        resolve: {
            extensions: ['.ts', '.tsx', '.js', '.jsx'],
            alias: {
                root: process.cwd()
            }
        },

        plugins: [
            new AssetMapPlugin({ dir: root + config.buildDir }),
            new webpack.HashedModuleIdsPlugin(),
        ]
    }

    if (isDev) {
        webpackConfig.devtool = 'source-map'
    }

    return webpackConfig
}

export default createWebpackConfig
